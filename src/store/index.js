import React, {createContext, useReducer} from 'react';

const initialState = {
  message:"Hello world!"
};
const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ( { children } ) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch(action.type) {
      case 'SET_MESSAGE':
        return {
          ...state,
          message:`Hello ${action.data}`
        };
      default:
        throw new Error("INVALID_ACTION");
    };
  }, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider }