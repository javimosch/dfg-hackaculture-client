import styled from '@emotion/styled'

export const Page = styled.div `
  max-width:900px;
  margin:0 auto;
  padding:20px;
`