import React from "react";

import { BrowserRouter as Router } from "react-router-dom";
import { AuthProvider } from "./lib/Auth";
import App from "./App";

import { I18nextProvider } from 'react-i18next';
import i18n from './i18n';

import 'bulma/css/bulma.css'
import "./App.css";

import { hydrate, render } from "react-dom";
import { StateProvider } from './store/index';

var Root = (<Router>
  <I18nextProvider i18n={i18n}>
    <StateProvider>
    <AuthProvider>
      <App />
    </AuthProvider>
    </StateProvider>
  </I18nextProvider>
</Router>)

const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
  hydrate(Root, rootElement);
} else {
  render(Root, rootElement);
}