import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withAuth } from "./../lib/Auth";
import userService from "./../lib/user-service"
import projectService from "./../lib/project-service"

import { render } from "@testing-library/react";

import styled from '@emotion/styled'

const UserLink = styled.div`
  
  a{
    text-decoration:none!important;
  }
  a:hover .card{
    opacity:0.8;
  }
`

class AcceptedParticipantsCard extends Component{
    constructor(){
        super()
        this.state = {
            ...this.state,
            avatarSrc:"https://secondchancetinyhomes.org/wp-content/uploads/2016/09/empty-profile.png"
        }
    }
    componentDidMount(){
       this.mountRandomAvatar()
    }
    mountRandomAvatar(){
        let random = Math.floor(Math.random() * 16) + 1  
        import(`../assets/avatars/av${random}.jpg`).then(module=>{
            this.setState({
                avatarSrc:module.default
            })
        })
    }
    getName(){
        const {firstName, email} = this.props
        return firstName || (email.split('@').length>0&&email.split('@')[0])
    }
    render() {
        return (
            <UserLink>
                
                <Link to={`/see-user-detail/${this.props._id}`}>
                <div className="card">
                    <div className="card-content">
                        <div className="media">
                            <div className="media-left">
                                <figure className="image is-48x48">
                                    <img src={this.state.avatarSrc} alt="Placeholder image"></img>
                                </figure>
                            </div>
                            <div className="media-content">
                                <p className="title is-5">{ this.getName() }</p>
                                <p className="subtitle is-6">{ this.props.skills }</p>
                            </div>
                        </div>
                    </div>
                </div>

                </Link>
                

            </UserLink>
        )
    }

} 

export default withAuth(AcceptedParticipantsCard)