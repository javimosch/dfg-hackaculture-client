import React, { Component } from "react";
import { withAuth } from "./../lib/Auth";
import { Link } from "react-router-dom";
import styled from '@emotion/styled'
import InititateImage from '..//assets/img/user_portal_initiate.jpg'
import ParticipateImage from '..//assets/img/user_portal_participate.jpg'
import SeekImage from '..//assets/img/user_portal_seek.jpg'

const Logo = styled.img`
  max-width:350px;
  max-height:initial!important;
`

const Items = styled.div`
  display:flex;
  a{
    font-size:35px;
    color:#3273dc; 
  }
  a:hover{
    color:#30bd5f;
  }
`
const Item = styled.div`
padding: 20px;
border: 1px solid #0000000f;
margin: 5px;
  
`

const ItemIcon = styled.div`
min-width: 256px;
min-height: 256px;
`


const InitiateIcon = styled(ItemIcon)`
  background: url(${InititateImage});
  background-position: center;
  background-size: cover;
  background-position-x: -11px;
`

const ParticipateIcon = styled(ItemIcon)`
  background: url(${ParticipateImage});
  background-position: center;
  background-size: cover;
  background-position-x: -50px;
`
const SeekIcon = styled(ItemIcon)`
  background: url(${SeekImage});
  background-position: center;
  background-size: cover;
  background-position-x: -65px;
`


class UserPortal extends Component {

  render() {
    return (
      <div className="page">
        <div className="home-portal">
          <Items>
            <Link to={"/initiator-dashboard"}>
              <Item>
                <InitiateIcon></InitiateIcon>
                Initiate
                </Item>
            </Link>
            <Link to={"/participant-seek-project"} >
              <Item>
                <ParticipateIcon/>
                Participate
                </Item>
            </Link>
            <Link to={"/seek-users"} >
              <Item>
                <SeekIcon/>
                Seek for Skills
                </Item>
            </Link>
          </Items>
        </div>
      </div>
    );
  }
}

export default withAuth(UserPortal);
