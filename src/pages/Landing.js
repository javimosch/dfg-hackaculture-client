import React from 'react'
import styled from '@emotion/styled'
import { Link } from "react-router-dom";
import { withTranslation, Trans } from 'react-i18next';
import {DiscoverButton} from '../styled/buttons'
const Logo = styled.img`
  max-width:350px;
  max-height:initial!important;
`

const LandingBox = styled.div`
    @media (min-width: 992px) {
        max-width:900px;
        margin:0 auto;
    }
`

const TheCollective = styled.h3`
    margin-top:25px;
`

const MainText = styled.p`
    margin-top:20px;
`

const WideButton = styled.div`
    width:200px;
`

const OurSolution = styled.h3`
    margin-top:50px;
`

function Landing({ t }) {

  return (
    <LandingBox className="home-portal">

      <Logo src="https://res.cloudinary.com/misitioba/image/upload/v1585331187/dfg/dfg_logo.svg" alt="homeLogo" />
      <div className="home-portal-paragraph">
        <TheCollective className=" is-size-3">{t('The Collective')}</TheCollective>
        <MainText className=" is-size-6  has-text-weight-light has-text-justified">
          {t('Our Collective Text')}
        </MainText>

        <OurSolution className=" is-size-3">{t('Our solution')}</OurSolution>
        <MainText className=" is-size-6 has-text-weight-light has-text-justified">


          <Trans i18nKey="Our Solution Text">

          </Trans>

        </MainText>

        <DiscoverButton to="/home">
          {t("Discover")}
        </DiscoverButton>

      </div>
    </LandingBox>
  )
}


export default withTranslation('translations')(Landing);
